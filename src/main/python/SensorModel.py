'''
Created on Sep 4, 2015

@author: chandra
'''

from ghmm import *


K_file = "/home/chandra/Documents/Data/SequenceFiles/K_Data_Sequence_File.seq"
AD_file = "/home/chandra/Documents/Data/SequenceFiles/AD_Data_Sequence_File.seq"
AK_file = "/home/chandra/Documents/Data/SequenceFiles/AK_Data_Sequence_File.seq"
B_file = "/home/chandra/Documents/Data/SequenceFiles/B_Data_Sequence_File.seq"
C_file = "/home/chandra/Documents/Data/SequenceFiles/C_Data_Sequence_File.seq"
D_file = "/home/chandra/Documents/Data/SequenceFiles/D_Data_Sequence_File.seq"

#two states
# A = [[0.14285715,0.85714285], [0.334565,0.665435]]
# B = [[0.3145142,0.6854858]
#      ,[0.1,0.9]]
# pi = [0.24685,0.75315]
A = [[0.14285715,0.85714285], [0.334565,0.665435]]
# B = [[0.3145142,0.6854858]
#      ,[0.1,0.9]]
B = [[0.05319149,0.03723404,0.010638298,0.026595745,0.005319149,0.06382979,0.026595745,0.031914894,0.07446808,0.08510638,0.10106383,0.09042553,0.005319149,0.06382979,0.09042553,0.005319149,0.06382979,0.08510638,0.031914894,0.047872342],
     [0.10285714,0.08571429,0.10285714,0.0057142857,0.04,0.022857143,0.057142857,0.057142857,0.04,0.074285716,0.05142857,0.0057142857,0.011428571,0.0057142857,0.022857143,0.0057142857,0.057142857,0.114285715,0.05142857,0.08571429]
     ]
pi = [0.24685,0.75315]
# three states
# A = [[0.14285715,0.42857143,0.42857143], [0.375,0.25,0.375],[0.5,0.25,0.25]]
# B=[[0.4,0.6],
#       [0.3,0.7],
#       [0.25,0.75]]
# pi = [0.2,0.6,0.2]
#four states
# A = [[0.4,0.4, 0.1,0.1], [0.3, 0.1,0.4,0.2],[0.2,0.1,0.4,0.3],[0.5, 0.1,0.2,0.2]]
# B = [[0.2,0.6,0.2],
#      [0.2857143,0.42857143,0.2857143],
#      [0.33333334,0.5,0.16666667],
#      [0.25,0.375,0.375]]
# pi = [0.3,0.3,0.2,0.2]
#five states
#A = [[0.4,0.2,0.2, 0.1,0.1], [0.3, 0.1,0.31,0.09,0.2],[0.2,0.1,0.12,0.28,0.3],[0.35,0.15, 0.1,0.2,0.2],[0.25,0.25, 0.1,0.2,0.2]]
#B = [[0.04,0.01,0.01,0.06,0.05,0.03,0.02,0.04,0.06,0.04,0.01,0.01,0.06,0.05,0.03,0.02,0.04,0.060,0.04,0.03,0.01,0.06,0.05,0.03,0.04,0.04,0.06],
#     [0.015,0.005,0.07,0.01,0.03,0.07,0.01,0.02,0.09,0.015,0.005,0.07,0.01,0.03,0.07,0.01,0.02,0.09,0.015,0.005,0.07,0.05,0.03,0.07,0.01,0.02,0.09],
#     p1,
#     [0.02,0.01,0.04,0.03,0.06,0.05,0.07,0.01,0.03,0.02,0.01,0.04,0.03,0.06,0.05,0.07,0.01,0.03,0.02,0.01,0.04,0.03,0.06,0.05,0.07,0.04,0.04],
#     [0.02,0.01,0.04,0.04,0.02,0.09,0.06,0.01,0.03,0.02,0.01,0.04,0.03,0.06,0.04,0.08,0.01,0.03,0.02,0.01,0.04,0.03,0.06,0.08,0.04,0.05,0.03]]
#pi = [0.2,0.2,0.2,0.2,0.2] 
sigma = IntegerRange(0,20)
m = HMMFromMatrices(sigma, DiscreteDistribution(sigma), A, B, pi)
file_location=K_file
# print m
#file = open('/home/chandra/Documents/sseqf.seq','r')
file_read = open(file_location,'r')
observations=[]
observations=file_read.readline().split(",")
values=[int(x) for x in observations if x.isdigit()]
train_seq=EmissionSequence(sigma,values)
m.baumWelch(train_seq)
print m
# abc=str(m);
# pp=[]
# inner2=[]
# inner5=[]
# inner7=[]
# inner11=[]
# pp=abc.split("\n")
# inner2=filter(None,re(pp[2].split(" ")))
# inner5=filter(None,re(pp[5].split(" ")))
# inner7=filter(None,re(pp[8].split(" ")))
# inner11=filter(None,re(pp[11].split(" ")))
# 
# pp[2]=','.join(inner2)
# pp[5]=','.join(inner5)
# pp[8]=','.join(inner7)
# pp[11]=','.join(inner11)
# 
# print "[AD,AK,D]"
# for st in pp:
#     print st
    
#v=m.viterbi(train_seq)
#print v


