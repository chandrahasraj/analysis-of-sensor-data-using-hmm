import matplotlib.pyplot
import pylab
import time
from calendar import timegm
import glob,os

files = glob.glob("C:\\Users\\chand\\Dropbox\\Research_Data/GraphFiles/AK_*.csv")
plt = matplotlib.pyplot
fig = plt.figure(figsize=(10, 10))

col =1
row = 2
for file in files:
    x = []
    y = []
    firstline = True
    open_file = open(file, 'r')
    file_name,f = os.path.splitext(open_file.name)
    title = os.path.basename(file_name)
    data = [line.strip() for line in open_file]
    xlabel = "Day number in year"
    ylabel = "Magnitude in cm/s"

    for line in data:
        if not firstline:
            x.append(line.split("=")[0])
            y.append(line.split("=")[1])
        firstline = False
    plt.subplot(3,3,col) # one row, one column, first plot
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.scatter(x,y)
    col = col + 1


plt.show()