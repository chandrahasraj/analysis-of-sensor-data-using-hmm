package student.sensors;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;

public class UsageMethods implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	static File serialized_files;
	static File directory;

	static {
		if (SystemUtils.IS_OS_WINDOWS) {
			directory = new File("C:\\Users\\chand\\Dropbox\\Research_Data\\Sub-Data");
			serialized_files = new File("C:\\Users\\chand\\Dropbox\\Research_Data\\SerializedFiles");
		} else {
			directory = new File("/home/chandra/Documents/Data/SerializedObjects");
			serialized_files = new File("/home/chandra/Documents/Data/Sub-Data");
		}
	}

	File findSerializedFile(String sensor_file) {
		File serialized_file = new File(serialized_files, sensor_file + "_serialized.txt");
		return serialized_file;
	}

	void serializeFile(File sensor_file, List<UsageMethods> obj) {
		File to_serialize = findSerializedFile(getFileName(sensor_file));
		try {
			if (!to_serialize.exists())
				to_serialize.createNewFile();
			FileOutputStream stream = new FileOutputStream(to_serialize);
			ObjectOutputStream out = new ObjectOutputStream(stream);
			out.writeObject(obj);
			out.close();
			stream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	List<UsageMethods> deserializeFile(File sensor_file) {
		File to_serialize = findSerializedFile(getFileName(sensor_file));
		List<UsageMethods> objs = new ArrayList<>();
		if (to_serialize.exists()) {
			try {
				FileInputStream stream = new FileInputStream(to_serialize);
				ObjectInputStream out = new ObjectInputStream(stream);
				objs = (List<UsageMethods>) out.readObject();
				out.close();
				stream.close();
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return objs;
	}

	UsageMethods chooseObjectType(String sensor_name, float vn, float ve, Date date) {
		switch (sensor_name) {
		case "AK_Data": {
			return new SensorAK(vn, ve, date);
		}
		case "D_Data": {
			return new SensorD(vn, ve, date);
		}
		case "B_Data": {
			return new SensorB(vn, ve, date);
		}
		case "K_Data": {
			return new SensorK(vn, ve, date);
		}
		case "C_Data": {
			return new SensorC(vn, ve, date);
		}
		case "AD_Data": {
			return new SensorAD(vn, ve, date);
		}
		}
		return null;
	}

	List<UsageMethods> createSensorObjects(File sensor_file, String sensor_name) {
		List<UsageMethods> objects = new ArrayList<>();
		// Set<Double> test = new TreeSet<>();
		try {
			List<String> lines = FileUtils.readLines(sensor_file);
			for (int i = 1; i < lines.size(); i++) {
				String values[] = lines.get(i).split(",");
				if (!values[22].trim().equalsIgnoreCase("NA") && !values[23].trim().equalsIgnoreCase("NA")
						&& (values[8] != null && !values[8].isEmpty())) {
					float vn = Float.parseFloat(values[22]);
					float ve = Float.parseFloat(values[23]);
					String date_time = values[8];
					Date date = sdf.parse(date_time);
					UsageMethods object = chooseObjectType(sensor_name, vn, ve, date);
					objects.add(object);
					// SensorB t=(SensorB)object;
					// test.add(t.getMag());
				}
			}
			// System.out.println(test);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return objects;
	}

	String getFileName(File sensor_file) {
		if (sensor_file.getName().startsWith("AD_Data")) {
			return "AD_Data";
		} else if (sensor_file.getName().startsWith("K_Data")) {
			return "K_Data";
		} else if (sensor_file.getName().startsWith("AK_Data")) {
			return "AK_Data";
		} else if (sensor_file.getName().startsWith("B_Data")) {
			return "B_Data";
		} else if (sensor_file.getName().startsWith("C_Data")) {
			return "C_Data";
		} else {
			return "D_Data";
		}
	}

	double calculateDegree(float vn, float ve) {
		double deg;
		if (ve != 0) {
			deg = Math.toDegrees(Math.atan(vn / ve));
			if (vn < 0 && ve < 0)// 3rd
				deg = 270 - deg;
			if (vn < 0 && ve > 0)// 2nd
				deg = 360 - deg + 90;
			if (vn > 0 && ve > 0)// 1st
				deg = 90 - deg;
			if (vn > 0 && ve < 0)// 4th
				deg = 360 - deg + 270;

			deg = deg > 360 ? deg % 360 : deg < 0 ? (deg % 360) + 360 : deg;
			// if (deg < 0 && deg > 360)
			// System.out.println(deg);
		} else {
			deg = vn > 0 ? 0 : 180;
		}
		return deg;
	}

	int getInFlowAndOutFlowDegrees(String sensor, double deg) {
		switch (sensor) {
		case "AK_Data": {
			return deg > 270 || deg < 90 ? 1 : 0;
		}
		case "D_Data": {
			return deg > 45 && deg < 225 ? 1 : 0;
		}
		case "B_Data": {
			return deg < 180 ? 0 : 1;
		}
		case "K_Data": {
			return deg < 180 ? 1 : 0;
		}
		case "C_Data": {
			return deg < 90 || deg > 270 ? 1 : 0;
		}
		case "AD_Data": {
			return deg < 135 || deg > 300 ? 1 : 0;
		}
		}
		return -1;
	}

	double getModifiedMag(double mag, int direction) {
		return direction == 1 ? mag : (-1) * mag;
	}

	static List<UsageMethods> getADData() {
		UsageMethods method = new UsageMethods();
		File file = new File(directory, "AD_Data_1.csv");
		List<UsageMethods> objects = new ArrayList<>();
		// List<UsageMethods> objects = method.deserializeFile(file);
		if (objects.isEmpty()) {
			objects = method.createSensorObjects(file, method.getFileName(file));
//			method.serializeFile(file, objects);
		}
		return objects;
	}

	static List<UsageMethods> getBData() {
		UsageMethods method = new UsageMethods();
		File file = new File(directory, "B_Data_4.csv");
		List<UsageMethods> objects = new ArrayList<>();
		// List<UsageMethods> objects = method.deserializeFile(file);
		if (objects.isEmpty()) {
			objects = method.createSensorObjects(file, method.getFileName(file));
//			method.serializeFile(file, objects);
		}
		return objects;
	}

	static List<UsageMethods> getCData() {
		UsageMethods method = new UsageMethods();
		File file = new File(directory, "C_Data_5.csv");
		List<UsageMethods> objects = new ArrayList<>();
		// List<UsageMethods> objects = method.deserializeFile(file);
		if (objects.isEmpty()) {
			objects = method.createSensorObjects(file, method.getFileName(file));
//			method.serializeFile(file, objects);
		}
		return objects;
	}

	static List<UsageMethods> getAKData() {
		UsageMethods method = new UsageMethods();
		File to_serialize_sensor = new File("AK_Data");
		List<UsageMethods> objects = new ArrayList<>();
		// List<UsageMethods> objects =
		// method.deserializeFile(to_serialize_sensor);
		boolean createSerializedFile = false;
		if (objects.isEmpty()) {
			for (File file : directory.listFiles()) {
				if (file.getName().startsWith("AK_Data")) {
					createSerializedFile = true;
					objects.addAll(method.createSensorObjects(file, method.getFileName(file)));
				}
			}
		}
//		if (createSerializedFile)
//			method.serializeFile(to_serialize_sensor, objects);
		return objects;
	}

	static List<UsageMethods> getKData() {
		UsageMethods method = new UsageMethods();
		File to_serialize_sensor = new File("K_Data");
		List<UsageMethods> objects = new ArrayList<>();
		// List<UsageMethods> objects =
		// method.deserializeFile(to_serialize_sensor);
		boolean createSerializedFile = false;
		if (objects.isEmpty()) {
			for (File file : directory.listFiles()) {
				if (file.getName().startsWith("K_Data")) {
					createSerializedFile = true;
					objects.addAll(method.createSensorObjects(file, method.getFileName(file)));
				}
			}
		}
//		if (createSerializedFile)
//			method.serializeFile(to_serialize_sensor, objects);
		return objects;
	}

	static List<UsageMethods> getDData() {
		File to_serialize_sensor = new File("D_Data");
		UsageMethods method = new UsageMethods();
		List<UsageMethods> objects = new ArrayList<>();
		// List<UsageMethods> objects =
		// method.deserializeFile(to_serialize_sensor);
		boolean createSerializedFile = false;
		if (objects.isEmpty()) {
			for (File file : directory.listFiles()) {
				if (file.getName().startsWith("D_Data")) {
					createSerializedFile = true;
					objects.addAll(method.createSensorObjects(file, method.getFileName(file)));
				}
			}
		}
//		if (createSerializedFile)
//			method.serializeFile(to_serialize_sensor, objects);
		return objects;
	}

	static void saveMap(Map<Integer, Integer> map, File file) {
		Properties properties = new Properties();

		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			properties.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
		}

		try {
			properties.store(new FileOutputStream(file), null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
