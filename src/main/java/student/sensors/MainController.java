package student.sensors;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

public class MainController {

	static File seq_file_dir = new File("/home/chandra/Documents/Data/SequenceFiles");

	public static void main(String as[]) throws IOException, ParseException {
		List<UsageMethods> objects = UsageMethods.getBData();
		File seq_file = null;
		String type ="";
		System.out.println(objects.size());
		List<Integer> magnitudes = new ArrayList<>();
		int max = -1;
		for (UsageMethods object : objects) {
			if (object.getClass().isAssignableFrom(SensorAD.class)) {
				SensorAD sensor = (SensorAD) object;
				seq_file = new File(seq_file_dir, "AD_Data_Sequence_File.seq");
				int modified_mag = (int) sensor.getModified_mag();
				if (modified_mag <= 5)
					magnitudes.add(0);
				else
					magnitudes.add(1);
				if (modified_mag > max)
					max = modified_mag;
				type = "AD";
			} else if (object.getClass().isAssignableFrom(SensorB.class)) {
				SensorB sensor = (SensorB) object;
				seq_file = new File(seq_file_dir, "B_Data_Sequence_File.seq");
				int modified_mag = (int) sensor.getModified_mag();
//				if (modified_mag <2)
//					magnitudes.add(0);
//				else
					magnitudes.add(modified_mag);
					if (modified_mag > max){
						max = modified_mag;
//						System.out.println(modified_mag);
					}
					type = "B";
			} else if (object.getClass().isAssignableFrom(SensorK.class)) {
				SensorK sensor = (SensorK) object;
				seq_file = new File(seq_file_dir, "K_Data_Sequence_File.seq");
				int modified_mag = (int) sensor.getModified_mag();
				if (modified_mag < 4)
					magnitudes.add(0);
				else
					magnitudes.add(1);
				if (modified_mag > max)
					max = modified_mag;
				type = "K";
			}
		}
		System.out.println(max);
		System.out.println(type);
		FileUtils.writeStringToFile(seq_file, StringUtils.join(magnitudes, ","));
	}

}
