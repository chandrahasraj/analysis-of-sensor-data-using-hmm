package student.sensors;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

public class TestController {
	static File seq_file_dir = new File("/home/chandra/Documents/Data/SequenceFiles");

	public static void main(String as[]) throws IOException, ParseException {
		File csv_ad = new File(seq_file_dir, "K_Sequence_File_Test.seq");
		List<UsageMethods> objects = UsageMethods.getKData();
		System.out.println(objects.size());
		List<Integer> magnitudes = new ArrayList<>();
		for (UsageMethods object : objects) {
			if (object.getClass().isAssignableFrom(SensorAD.class)) {
				SensorAD ad = (SensorAD) object;
				int modified_mag = (int) ad.getModified_mag();
				if (modified_mag < 9)
					magnitudes.add(0);
				else
					magnitudes.add(1);
			}else if (object.getClass().isAssignableFrom(SensorK.class)) {
				SensorK ad = (SensorK) object;
				int modified_mag = (int) ad.getModified_mag();
				if (modified_mag < 4)
					magnitudes.add(0);
				else
					magnitudes.add(1);
			}
		}
		
		Collections.shuffle(magnitudes);
		List<Integer> list = new ArrayList<>();
		for(int i=0;i<(magnitudes.size()/10);i++)
			list.add(magnitudes.get(i));
		
		FileUtils.writeStringToFile(csv_ad, StringUtils.join(list, ","));
	}
}
