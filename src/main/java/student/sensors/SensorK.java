package student.sensors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class SensorK extends UsageMethods implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5942959699404318902L;
	private float vn, ve;
	private Date date;
	private double mag, deg, modified_mag;
	private int direction;
	String sensor_name;
	private static Map<Integer,List<Integer>> day_data = new TreeMap<>();

	public SensorK(float vn, float ve, Date date) {
		super();
		this.sensor_name = "K_Data";
		this.vn = vn;
		this.ve = ve;
		this.date = date;
		this.mag = Math.round(Math.sqrt((vn * vn) + (ve * ve)));
		this.deg = calculateDegree(vn, ve);
		this.direction = getInFlowAndOutFlowDegrees(sensor_name, deg);
		this.modified_mag = getModifiedMag(mag, direction);
		setDay_data();
	}

	private void setDay_data() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int key = calendar.get(Calendar.DAY_OF_YEAR) + calendar.get(Calendar.YEAR)*1000;
		List<Integer> value = new ArrayList<>();
		if(day_data.containsKey(key))
			value.addAll(day_data.get(key));
		
		value.add((int)modified_mag);
		
		day_data.put(key, value);
	}
	
	public static Map<Integer, List<Integer>> getDay_data() {
		return day_data;
	}
	
	public float getVn() {
		return vn;
	}

	public float getVe() {
		return ve;
	}

	public Date getDate() {
		return date;
	}

	public double getMag() {
		return mag;
	}

	public double getDeg() {
		return deg;
	}

	public int getDirection() {
		return direction;
	}
	
	public double getModified_mag() {
		return modified_mag;
	}
}
