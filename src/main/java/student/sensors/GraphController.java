package student.sensors;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.SystemUtils;

public class GraphController {
	static File graph_file_dir;

	static {
		if (SystemUtils.IS_OS_WINDOWS)
			graph_file_dir = new File("C:\\Users\\chand\\Dropbox\\Research_Data/GraphFiles");
		else
			graph_file_dir = new File("/home/chandra/Documents/Data/GraphFiles");
	}

	public static void main(String as[]) throws IOException, ParseException {
		UsageMethods.getAKData();
		Map<Integer, List<Integer>> map = SensorAK.getDay_data();
		createFiles(map, "AK_Data_Graph_File");
	}

	static void createFiles(Map<Integer, List<Integer>> map, String f_name) {
		Map<Integer, Integer> f_map = new TreeMap<>();
		File graph_file = null;
//		System.out.println(map);
		int cur_year=2006,prev_year=2006;
		for (Map.Entry<Integer, List<Integer>> object : map.entrySet()) {
			int sum = 0;
			int key = object.getKey();
			cur_year = key/1000;
			for (Integer ii : object.getValue())
				sum += ii;
			int value = (sum / object.getValue().size());
			if(cur_year != prev_year){
				graph_file = new File(graph_file_dir, f_name+"_"+prev_year+".csv");
				prev_year = cur_year;
				UsageMethods.saveMap(f_map, graph_file);
				f_map.clear();
			}
			f_map.put(key%1000, value);
		}
		graph_file = new File(graph_file_dir, f_name+"_"+prev_year+".csv");
		UsageMethods.saveMap(f_map, graph_file);
		f_map.clear();
		
	}
}
