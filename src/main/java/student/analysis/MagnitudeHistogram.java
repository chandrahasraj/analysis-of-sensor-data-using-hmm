package student.analysis;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;

public class MagnitudeHistogram extends Constants{

	static TreeMap<Integer,Integer> inflow_map = new TreeMap<>();
	static TreeMap<Integer,Integer> outflow_map = new TreeMap<>();
	static TreeMap<String,TreeMap<Integer,Integer>> sensor_in_map=new TreeMap<>();
	static TreeMap<String,TreeMap<Integer,Integer>> sensor_out_map=new TreeMap<>();
	
	public static void main(String[] args) throws IOException {
		File directory = new File("C:\\Users\\chand\\Documents\\Workspace\\Data\\Research_Data");
		
		for (File file : directory.listFiles()) {
			if (file.getName().endsWith(".csv")) {
				String file_start_name = file.getName().substring(0, file.getName().length() - 4);
				List<String> lines = FileUtils.readLines(file);
				for (int i = 1; i < lines.size(); i++) {
					if (!(lines.get(i).split(",")[22].equals("NA") || lines.get(i).split(",")[22] == null)) {
						float vn = Float.parseFloat(lines.get(i).split(",")[22]);
						float ve = Float.parseFloat(lines.get(i).split(",")[23]);
						int deg = (int) calculateDegree(vn, ve);
						int mag = (int)calculateMagnitude(vn, ve);
						
						insert_into_map(deg,mag,file_start_name);
					}
				}
				

				if (file_start_name.startsWith("AD")) {
					store_in_sensor_map("AD");
				} else if (file_start_name.startsWith("AK")) {
					store_in_sensor_map("AK");
				} else if (file_start_name.startsWith("D")) {
					store_in_sensor_map("D");
				} else if (file_start_name.startsWith("C")) {
					store_in_sensor_map("C");
				} else if (file_start_name.startsWith("B")) {
					store_in_sensor_map("B");
				} else if (file_start_name.startsWith("K")) {
					store_in_sensor_map("K");
				}

			}
			
			for(Map.Entry<String,TreeMap<Integer,Integer>> entry:sensor_in_map.entrySet()){
				String file_name = entry.getKey();
				File s_file = new File(directory,file_name+"_in.txt");
				List<String> lines = new ArrayList<>();
				for(Map.Entry<Integer,Integer> e:entry.getValue().entrySet()){
					String line = e.getKey()+","+e.getValue();
					lines.add(line);
				}
				FileUtils.writeLines(s_file, lines);
			}
			for(Map.Entry<String,TreeMap<Integer,Integer>> entry:sensor_out_map.entrySet()){
				String file_name = entry.getKey();
				File s_file = new File(directory,file_name+"_out.txt");
				List<String> lines = new ArrayList<>();
				for(Map.Entry<Integer,Integer> e:entry.getValue().entrySet()){
					String line = e.getKey()+","+e.getValue();
					lines.add(line);
				}
				FileUtils.writeLines(s_file, lines);
			}
	}

	}
	
	private static void store_in_sensor_map(String sensor_name){
		TreeMap<Integer,Integer> temp_in = new TreeMap<>();
		TreeMap<Integer,Integer> temp_out = new TreeMap<>();
		TreeMap<Integer,Integer> f_temp_in = new TreeMap<>();
		TreeMap<Integer,Integer> f_temp_out = new TreeMap<>();
		
		if(sensor_in_map.containsKey(sensor_name))
			temp_in = sensor_in_map.get(sensor_name);
		if(sensor_out_map.containsKey(sensor_name))
			temp_out = sensor_out_map.get(sensor_name);
		f_temp_in = add_maps(temp_in, inflow_map);
		f_temp_out = add_maps(temp_out, outflow_map);
		sensor_in_map.put(sensor_name, f_temp_in);
		sensor_out_map.put(sensor_name, f_temp_out);
		inflow_map.clear();
		outflow_map.clear();
	}
	
	
	private static TreeMap<Integer, Integer> add_maps(TreeMap<Integer, Integer> map1, TreeMap<Integer, Integer> map2) {
		TreeMap<Integer,Integer> result= new TreeMap<>();
		if(map1.isEmpty())
			result.putAll(map2);
		else if(map2.isEmpty())
			result.putAll(map1);
		else{
			result.putAll(map2);
			for(Map.Entry<Integer,Integer> e:result.entrySet()){
				int v=0;
				if(map1.containsKey(e.getKey()))
					v=map1.get(e.getKey());
				result.put(e.getKey(), e.getValue()+v);
			}
		}
		return result;
	}

	private static void insert_into_map(int deg, int mag, String file_start_name) {
		int mag_count=0;
		if (file_start_name.startsWith("AD")) {
			if(deg<135||deg>300){
				if(outflow_map.containsKey(mag))
					mag_count=outflow_map.get(mag);
				outflow_map.put(mag, ++mag_count);
			}else{
				if(inflow_map.containsKey(mag))
					mag_count=inflow_map.get(mag);
				inflow_map.put(mag, ++mag_count);
			}
		} else if (file_start_name.startsWith("AK")) {
			if(deg>270||deg<90){
				if(outflow_map.containsKey(mag))
					mag_count=outflow_map.get(mag);
				outflow_map.put(mag, ++mag_count);
			}else{
				if(inflow_map.containsKey(mag))
					mag_count=inflow_map.get(mag);
				inflow_map.put(mag, ++mag_count);
			}
		} else if (file_start_name.startsWith("D")) {
			if(deg>45&&deg<225){
				if(outflow_map.containsKey(mag))
					mag_count=outflow_map.get(mag);
				outflow_map.put(mag, ++mag_count);
			}else{
				if(inflow_map.containsKey(mag))
					mag_count=inflow_map.get(mag);
				inflow_map.put(mag, ++mag_count);
			}
		} else if (file_start_name.startsWith("C")) {
			if(deg<90||deg>270){
				if(outflow_map.containsKey(mag))
					mag_count=outflow_map.get(mag);
				outflow_map.put(mag, ++mag_count);
			}else{
				if(inflow_map.containsKey(mag))
					mag_count=inflow_map.get(mag);
				inflow_map.put(mag, ++mag_count);
			}
		} else if (file_start_name.startsWith("B")) {
			if(deg>180){
				if(outflow_map.containsKey(mag))
					mag_count=outflow_map.get(mag);
				outflow_map.put(mag, ++mag_count);
			}else{
				if(inflow_map.containsKey(mag))
					mag_count=inflow_map.get(mag);
				inflow_map.put(mag, ++mag_count);
			}
		} else if (file_start_name.startsWith("K")) {
			if(deg<180){
				if(outflow_map.containsKey(mag))
					mag_count=outflow_map.get(mag);
				outflow_map.put(mag, ++mag_count);
			}else{
				if(inflow_map.containsKey(mag))
					mag_count=inflow_map.get(mag);
				inflow_map.put(mag, ++mag_count);
			}
		}
	}

}