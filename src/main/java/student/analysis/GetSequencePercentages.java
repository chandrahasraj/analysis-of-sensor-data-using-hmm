package student.analysis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class GetSequencePercentages {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(
				new FileReader(new File("/home/chandra/Documents/Data/SequenceFiles/AD_Sequence_File.seq")));
		String line = reader.readLine();
		String values[] = line.split(",");
		float c1 = 0, c2 = 0, c3 = 0;
		for (String value : values) {
			int key = Integer.parseInt(value);
			if (key == 2)
				c2++;
			else if (key == 1)
				c1++;
			else
				c3++;

		}
		float total = c1 + c2 + c3;
		System.out.println("0:" + (c3 / total) + ",1:" + (c1 / total) + ",2:" + (c2 / total));
		reader.close();
	}

}
