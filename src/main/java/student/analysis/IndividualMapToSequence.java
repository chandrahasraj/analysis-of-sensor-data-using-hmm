package student.analysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

public class IndividualMapToSequence extends Constants {

	static List<Integer> training_data = new LinkedList<>();
	static List<Integer> testing_data = new LinkedList<>();

	static int begin = 5;

	public static void main(String as[]) throws IOException {
		List<Integer> interim_list = new LinkedList<>();
		for (File file : Map_directory.listFiles()) {
			if (file.getName().startsWith("K_Data")) {
				sequenceCalculator(file, interim_list);
			}
		}
//		FileUtils.writeStringToFile(test_file, StringUtils.join(interim_list, ","));
	}

	static void sequenceCalculator(File file, List<Integer> interim_list) {
		Properties prop = new Properties();

		try {
			prop.load(new FileInputStream(file));
			int count = prop.size() / 10, index = 1;

			System.out.println(count);
			System.out.println(prop.size());
			int max = 0;

			for (String key : prop.stringPropertyNames()) {
				// testing purpose
				String key_values[] = key.split("-");
				String interm_key = key_values[0];

				// int day_of_year = (int) (Long.valueOf(key_values[1]) /
				// 10000);
				// int hour_of_day = (int) ((Long.valueOf(key_values[1]) -
				// day_of_year * 10000) / 100);

//				if (interm_key.equals("2010")) 
				{
					String value = prop.get(key).toString();
					String mag_deg[] = value.split(",");
					float deg = Float.valueOf(mag_deg[1]);
					float mag = Float.valueOf(mag_deg[0]);
					max = max < mag ? (int) mag : max;
					//For AD_DATA
//					int bit = deg < 135 || deg > 300 ? (mag > 12.0 ? 0 : 2) : 1;
					//for K Data
					int bit = deg < 180 ? 1 : 0;
					
					interim_list.add(bit);
				}
			}
			// training_data.addAll(Training_List.createTrainingData(begin));
			// testing_data.addAll(Test_List.createTestData(begin));
			//
			// FileUtils.writeStringToFile(training_Seq,
			// StringUtils.join(training_data, ","));
			// FileUtils.writeStringToFile(testing_Seq,
			// StringUtils.join(testing_data, ","));
			System.out.println(max);

			// }
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static void setArrayBasedOnSensorName(String sensor, int[] array, float deg) {
		switch (sensor) {
		case "AK_Dat": {
			array[1] = deg > 270 || deg < 90 ? 1 : 0;
			break;
		}
		case "D_Data": {
			array[4] = deg > 45 && deg < 225 ? 1 : 0;
			break;
		}
		case "B_Data": {
			array[2] = deg < 180 ? 0 : 1;
			break;
		}
		case "K_Data": {
			array[1] = deg < 180 ? 1 : 0;
			break;
		}
		case "C_Data": {
			array[3] = deg < 90 || deg > 270 ? 1 : 0;
			break;
		}
		case "AD_Dat": {
			array[0] = deg < 135 || deg > 300 ? 1 : 0;
			break;
		}
		}
	}
}
