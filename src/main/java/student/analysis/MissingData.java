package student.analysis;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;

public class MissingData extends Constants{

	static TreeMap<Integer, Integer> map = new TreeMap<>();
	static TreeMap<Integer, TreeMap<Integer, Integer>> month_wise_data = new TreeMap<>();
	static TreeMap<String,TreeMap<Integer,Integer>> sensor_map = new TreeMap<>();
	static TreeMap<Integer, Integer> d_map = new TreeMap<>();
	static TreeMap<Integer,List<Integer>> mag_deg=new TreeMap<>();

	public static void main(String[] args) throws IOException, ParseException {
		File directory = new File("C:\\Users\\chand\\Documents\\Workspace\\Data\\Research_Data");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		int days[] = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		for (File file : directory.listFiles()) {
			if (file.getName().endsWith(".csv")) {
				String file_start_name = file.getName().substring(0, file.getName().length() - 4);
				List<String> lines = FileUtils.readLines(file);
				for (int i = 1; i < lines.size(); i++) {
					String date_time = lines.get(i).split(",")[8];
					if (!(lines.get(i).split(",")[22].equals("NA") || lines.get(i).split(",")[22] == null)) {
						float vn = Float.parseFloat(lines.get(i).split(",")[22]);
						float ve = Float.parseFloat(lines.get(i).split(",")[23]);
						int deg = (int) calculateDegree(vn, ve);
						int mag = (int)calculateMagnitude(vn, ve);
						List<Integer> list=new ArrayList<>();
						cal.setTime(sdf.parse(date_time));
						if (mag_deg.containsKey(deg)) {
							list = mag_deg.get(deg);
						}
						list.add(mag);
						mag_deg.put(deg, list);
					}
					// displayCount(cal);
				}
				d_map = sum_mag_deg(mag_deg);
				if (file_start_name.startsWith("AD")) {
					System.out.println("count of map AD");
					
					print_map(d_map);
//					System.out.println(d_map);
				} else if (file_start_name.startsWith("AK")) {
					System.out.println("count of map AK");
					print_map(d_map);
//					System.out.println(d_map);
				} else if (file_start_name.startsWith("D")) {
					System.out.println("count of map D");
					TreeMap<Integer,Integer> m=new TreeMap<>();
					TreeMap<Integer,Integer> m2=new TreeMap<>();
					if(sensor_map.containsKey("D")){
						m = sensor_map.get("D");
						TreeMap<Integer, Integer> n_map = add_maps(m,d_map);
						sensor_map.put("D", n_map);
						print_map(n_map);
					}else{
						m2.putAll(d_map);
						sensor_map.put("D", m2);
					}
//					print_map(d_map);
//					System.out.println(d_map);
				} else if (file_start_name.startsWith("C")) {
					System.out.println("count of map C");
					print_map(d_map);
//					System.out.println(d_map);
				} else if (file_start_name.startsWith("B")) {
					System.out.println("count of map B");
					print_map(d_map);
//					System.out.println(d_map);
				} else if (file_start_name.startsWith("K")) {
					System.out.println("count of map K");
					TreeMap<Integer,Integer> m=new TreeMap<>();
					TreeMap<Integer,Integer> m2=new TreeMap<>();
					if(sensor_map.containsKey("K")){
						m = sensor_map.get("K");
						TreeMap<Integer, Integer> n_map = add_maps(m,d_map);
						print_map(n_map);
					}
					m2.putAll(d_map);
					sensor_map.put("K", m2);
//					System.out.println(d_map);
				}
				d_map.clear();
			}
		}
	}
	
	private static TreeMap<Integer, Integer> sum_mag_deg(TreeMap<Integer, List<Integer>> mag_deg2) {
		TreeMap<Integer,Integer>map=new TreeMap<>();
		for(Map.Entry<Integer,List<Integer>> entry:mag_deg2.entrySet()){
			int sum =0;
			List<Integer> ll=entry.getValue();
			for(Integer ii:ll)
				sum+=ii;
			map.put(entry.getKey(), sum/ll.size());
		}
		return map;
	}

	private static TreeMap<Integer, Integer> add_maps(TreeMap<Integer, Integer> m, TreeMap<Integer, Integer> d_map2) {
		TreeMap<Integer,Integer> result= new TreeMap<>();
		result.putAll(m);
		for(Map.Entry<Integer,Integer> e:result.entrySet()){
			int v=0;
			if(d_map2.containsKey(e.getKey()))
				v=d_map.get(e.getKey());
			result.put(e.getKey(), e.getValue()+v);
		}
		return result;
	}

	private static void print_map(TreeMap<Integer, Integer> d_map2) {
		for(Map.Entry<Integer, Integer> entry:d_map2.entrySet()){
			System.out.println(entry.getKey()+","+entry.getValue());
		}
	}


	static void displayCount(Calendar cal) {
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int count = 0, day_count = 0;
		TreeMap<Integer, Integer> month_map = new TreeMap<>();
		if (map.containsKey(year)) {
			count = map.get(year);
		}
		if (month_wise_data.containsKey(year)) {
			month_map = month_wise_data.get(year);
			if (month_map.containsKey(month)) {
				day_count = month_map.get(month);
			}
		}
		map.put(year, ++count);
		month_map.put(month, ++day_count);
		month_wise_data.put(year, month_map);
	}
}
