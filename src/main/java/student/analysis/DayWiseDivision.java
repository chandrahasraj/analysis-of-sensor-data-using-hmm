package student.analysis;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;

public class DayWiseDivision extends Constants {
	public static void main(String as[]) throws IOException, ParseException {
		File files = new File(year_wise_division, "AD_YEAR_WISE");
		for (File file : files.listFiles()) {
			File year_directory[] = file.listFiles();
			for (File month : year_directory) {
				Map<String, String> month_wise_mag = new TreeMap<>();
				int name_len = month.getName().length();
				File month_days = new File(month.getParentFile(),month.getName().substring(0,name_len-11)+"_days.properties");
				List<String> lines = FileUtils.readLines(month);
				Calendar cal = Calendar.getInstance();
				lines.remove(0);
				String date = lines.get(0).split(",")[0];
				cal.setTime(day_format.parse(date));

				int cur_day = cal.get(Calendar.DAY_OF_MONTH), prev_day = cur_day;
				float sum = 0, count = 0;
				for (String line : lines) {
					date = line.split(",")[0];
					float mag = Float.valueOf(line.split(",")[1]);
					cal.setTime(day_format.parse(date));
					cur_day = cal.get(Calendar.DAY_OF_MONTH);
					if (prev_day != cur_day) {
						month_wise_mag.put(String.valueOf(prev_day), String.valueOf(sum / count));
						sum = 0;
						count = 0;
						prev_day = cur_day;
					}
					sum += mag;
					count++;
				}
				month_wise_mag.put(String.valueOf(prev_day), String.valueOf(sum / count));
				new Constants().saveMap(month_wise_mag, month_days);
			}
		}
	}
}
