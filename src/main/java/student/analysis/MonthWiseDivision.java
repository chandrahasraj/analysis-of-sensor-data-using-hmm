package student.analysis;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;

public class MonthWiseDivision extends Constants {

	public static void main(String as[]) throws IOException, ParseException {
		Constants constant = new Constants();
		File file = new File(directory, "AD_Data_1.csv");
		String file_start_name = file.getName().substring(0, 6);
		List<String> lines = FileUtils.readLines(file);
		Map<Date, Integer> days = new TreeMap<>();
		Map<Integer,Float> sd = new HashMap<>();
		boolean begining=true;
		int next_month = -1;
		for (int i = 1; i < lines.size(); i++) {
			if (!(lines.get(i).split(",")[22].equals("NA") || lines.get(i).split(",")[22] == null)) {
				float vn = Float.parseFloat(lines.get(i).split(",")[22]);
				float ve = Float.parseFloat(lines.get(i).split(",")[23]);
				String date_time = lines.get(i).split(",")[8];

				int deg = (int) calculateDegree(vn, ve);
				int mag = (int) calculateMagnitude(vn, ve);
				int cur_month = constant.getCalendarEntity(date_time, Calendar.MONTH);
				int year = constant.getCalendarEntity(date_time, Calendar.YEAR);
				int day = constant.getCalendarEntity(date_time, Calendar.DAY_OF_MONTH);
				int hour = constant.getCalendarEntity(date_time, Calendar.HOUR_OF_DAY);
				int minute = constant.getCalendarEntity(date_time, Calendar.MINUTE);
				int second = constant.getCalendarEntity(date_time, Calendar.SECOND);
				int direction = constant.setArrayBasedOnSensorName(file_start_name, deg);

				mag = direction == 1 ? mag : (-1) * mag;
				String hour_format = (hour/10) == 0 ? (String.format("0%d", hour)):String.valueOf(hour);
				String minute_format = (minute/10) == 0 ? (String.format("0%d", minute)):String.valueOf(minute);
				String second_format = (second/10) == 0 ? (String.format("0%d", second)):String.valueOf(second);

//				double key = Math.pow(10, 6) * day + Math.pow(10, 4) * hour + Math.pow(10, 2) * minute
//						+ Math.pow(10, 0) * second;
				String key = day+"/"+(cur_month+1)+"/"+year+" "+hour_format+":"+minute_format+":"+second_format;
//				String key = day+"/"+(cur_month+1)+"/"+year;
				Calendar cal = Calendar.getInstance();
				cal.setTime(sdf2.parse(key));
				days.put((Date) cal.getTime(), mag);
				
				if(begining){
					next_month = cur_month;
					begining = false;
				}
				
				if(next_month != cur_month){
					File month_directory = new File(directory,"YEAR_WISE/AD_YEAR_WISE/"+year);
					if(!month_directory.exists()){
						month_directory.mkdirs();
					}
					
					File month_file = new File(month_directory,constant.getMonthName(cur_month)+".properties");
					if(!month_file.exists())
						month_file.createNewFile();
					constant.saveMapProperties(days, month_file);
					next_month = cur_month;
					days.clear();
				}
			}
		}
	}
}
