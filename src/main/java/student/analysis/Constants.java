package student.analysis;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

public class Constants {
	final static Calendar calendar = Calendar.getInstance();
	final static File directory = new File("C:\\Users\\chand\\Documents\\Workspace\\Data\\Research_Data");
	final static File Map_directory=new File("C:\\Users\\chand\\Documents\\Workspace\\Data\\Research_Data/mapObjects");
	final static File year_wise_division=new File("C:\\Users\\chand\\Documents\\Workspace\\Data\\Research_Data/YEAR_WISE");
	final static File seq_file = new File(Map_directory,"test_five.seq");
	final static File training_set = new File(Map_directory,"training.seq");
	
	final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	final static SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	final static SimpleDateFormat day_format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
	static int totalCount=0;
	
	void saveMapToFile(Map<String,Set<String>> total_store,File file) {
		Properties properties = new Properties();

		for (Map.Entry<String,Set<String>> entry : total_store.entrySet()) {
		    properties.put(entry.getKey(), StringUtils.join(entry.getValue(),";"));
		}

		try {
			properties.store(new FileOutputStream(file), null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getCalendarEntity(String date_time,int entity){
		try {
			calendar.setTime(sdf.parse(date_time));
			return calendar.get(entity);
		} catch (ParseException e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	public int setArrayBasedOnSensorName(String sensor,double deg){
		switch(sensor){
			case "AK_Data":{
				return deg>270||deg<90?1:0;
			}
			case "D_Data":{
				return deg>45&&deg<225?1:0;
			}
			case "B_Data":{
				return deg<180?0:1;
			}
			case "K_Data":{
				return deg<180?1:0;
			}
			case "C_Data":{
				return deg<90||deg>270?1:0;
			}
			case "AD_Data":{
				return deg<135||deg>300?1:0;
			}
		}
		return -1;
	}
	
	void saveMap(Map<String,String> total_store,File file){
		Properties properties = new Properties();

		for (Map.Entry<String,String> entry : total_store.entrySet()) {
		    properties.put(entry.getKey(), entry.getValue());
		}

		try {
			properties.store(new FileOutputStream(file), null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void saveMapProperties(Map<Date,Integer> total_store,File file){
		List<String> lines = new ArrayList<>();

		for (Map.Entry<Date,Integer> entry : total_store.entrySet()) {
			StringBuilder builder = new StringBuilder();
//			long date = entry.getKey().getTime();
		    builder.append(String.valueOf(entry.getKey().toString())).append(",").append(String.valueOf(entry.getValue()));
		    lines.add(builder.toString());
		}
	    try {
			FileUtils.writeLines(file, lines, true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	String getMonthName(int month){
		switch(month){
		case 0: return "January";
		case 1: return "February";
		case 2: return "March";
		case 3: return "April";
		case 4: return "May";
		case 5: return "June";
		case 6: return "July";
		case 7: return "August";
		case 8: return "September";
		case 9: return "October";
		case 10: return "November";
		case 11: return "December";
		}
		return "";
	}
	
	protected static double calculateMagnitude(float vn, float ve) {
		return Math.round(Math.sqrt((vn * vn) + (ve * ve)));
	}

	protected static  double calculateDegree(float vn, float ve) {
			double deg;
			if (ve != 0) {
				deg = Math.toDegrees(Math.atan(vn / ve));
				if (vn < 0 && ve < 0)// 3rd
					deg = 270 - deg;
				if (vn < 0 && ve > 0)// 2nd
					deg = 360 - deg + 90;
				if (vn > 0 && ve > 0)// 1st
					deg = 90 - deg;
				if (vn > 0 && ve < 0)// 4th
					deg = 360 - deg + 270;

				deg = deg > 360 ? deg % 360 : deg < 0 ? (deg % 360) + 360 : deg;
				// if (deg < 0 && deg > 360)
				// System.out.println(deg);
			} else {
				deg = vn > 0 ? 0 : 180;
			}
			return deg;
		}
}
