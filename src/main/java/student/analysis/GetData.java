package student.analysis;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

public class GetData extends Constants {
	static File directory = new File("/home/chandra/Documents/Data/Sub-Data");
	static File save_directory = new File("/home/chandra/Documents/Data/SequenceFiles");

	public static void main(String[] args) throws IOException, ParseException {
		GetData data = new GetData();
		if (!save_directory.exists())
			save_directory.mkdir();
		for (File f : directory.listFiles()) {
			if (!f.isDirectory()) {
				if (f.getName().startsWith("AD_Data")) {
					File AD_Sequence_File = new File(save_directory, "AD_Sequence_File.seq");
					Map<Date, Integer> map = data.getADData(f);
					List<Integer> values = new ArrayList<Integer>(map.values());
					String value = StringUtils.join(values, ",");
					FileUtils.writeStringToFile(AD_Sequence_File, value);
				}
			}
		}
	}

	Map<Date, Integer> getADData(File AD) throws IOException, ParseException {
		List<String> data = FileUtils.readLines(AD);
		Map<Date, Integer> map = new TreeMap<>();
		Constants constant = new Constants();
		for (int i = 1; i < data.size(); i++) {
			String value = data.get(i);
			String values[] = value.split(",");
			if (!values[22].trim().equalsIgnoreCase("NA") && !values[23].trim().equalsIgnoreCase("NA")
					&& (values[8] != null && !values[8].isEmpty())) {

				float vn = Float.parseFloat(values[22]);
				float ve = Float.parseFloat(values[23]);

				String date_time = values[8];
				Date date = sdf.parse(date_time);
				double mag = calculateMagnitude(vn, ve);
				double deg = calculateDegree(vn, ve);
				int direction = constant.setArrayBasedOnSensorName("AD_Data", deg);
				mag = direction == 1 ? mag : (-1) * mag;
				int state = getADState(mag);
				map.put(date, state);
			}
		}
		return map;
	}

	int getADState(double mag) {
		if (mag >= 6.5)
			return 2;
		else if (mag < 6.5 && mag >= 1)
			return 1;
		else
			return 0;
	}
}
