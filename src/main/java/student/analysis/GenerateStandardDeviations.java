package student.analysis;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;

public class GenerateStandardDeviations extends Constants {

	public static void main(String as[]) {
		File year_directory = new File(directory, "YEAR_WISE/AD_YEAR_WISE");
		for (File file : year_directory.listFiles()) {
			for (File year : file.listFiles()) {
				if (year.getName().endsWith(".properties")) {
					List<String> sds = new ArrayList<>();
					try {
						List<String> lines = FileUtils.readLines(year);
						float mean = getMean(lines);
						double standarddeviation = calculateStandardDeviation(lines, mean);
						
//						System.out.format("%f,%f, %s \n", mean, standarddeviation,year.getParentFile().getName()+"/"+year.getName());
						float sd_1=0,sd_2=0,sd_3=0;
						for (String line : lines) {
							String key = line.split(",")[0];
							float value = Float.valueOf(line.split(",")[1]);
							int noOfSD = getStandardDeviation(value, standarddeviation, mean);
							
							if(noOfSD == 1)
								sd_1++;
							else if(noOfSD == 2)
								sd_2++;
							else
								sd_3++;
//							sds.add(key + "," + noOfSD);
						}
						System.out.println(year.getParentFile().getName()+"/"+year.getName()+","+((sd_1/lines.size())*100)+","
						+((sd_2/lines.size())*100)+","+((sd_3/lines.size())*100));
//						String path = year.getAbsolutePath();
////						System.out.println(year.getParent().substring(0, path.length() - 10) + "txt");
//						FileUtils.writeLines(new File(year.getAbsolutePath().substring(0, path.length() - 10) + "txt"),
//								sds, true);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	private static int getStandardDeviation(float value, double standarddeviation, float mean) {
		if ((value <= (mean + standarddeviation)) || value <= (Math.abs(mean - standarddeviation)))
			return 1;
		else if ((value <= (mean + 2 * standarddeviation)) || value <= (Math.abs(mean - 2 * standarddeviation)))
			return 2;
		else
			return 3;
	}

	private static double calculateStandardDeviation(List<String> lines, float mean) {
		float sd = 0.0f;
		for (String line : lines) {
			float value = Float.valueOf(line.split(",")[1]);
			sd += Math.pow((value - mean), 2);
		}

		return Math.sqrt((sd / lines.size()));
	}

	private static float getMean(List<String> lines) {
		float mean = 0.0f;
		for (String line : lines) {
			float value = Float.valueOf(line.split(",")[1]);
			mean += value;
		}
		return mean / lines.size();
	}
}
