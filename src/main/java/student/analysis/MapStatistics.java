package student.analysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

public class MapStatistics extends Constants {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		TreeMap<Integer, TreeMap<Integer, Integer>> map = new TreeMap<>();
		for (File file : Map_directory.listFiles()) {
			if (file.getName().startsWith("K_Data")) {
				Properties prop = new Properties();
				prop.load(new FileInputStream(file));
				for (String key : prop.stringPropertyNames()) {
					String value = prop.get(key).toString();
					String mag_deg[] = value.split(",");
					float deg = Float.valueOf(mag_deg[1]);
					float mag = Float.valueOf(mag_deg[0]);
					degree_blocks(deg, mag, map);
				}
			}
		}
		for (Map.Entry<Integer, TreeMap<Integer, Integer>> entry : map.entrySet()) {
			int key = entry.getKey();
			int tsum = 0;
			TreeMap<Integer, Integer> imap = entry.getValue();
			for (Map.Entry<Integer, Integer> e : imap.entrySet()) {
				int ikey = e.getKey();
				int ivalue = e.getValue();
				tsum += ivalue;
				System.out.println(ikey + "," + ivalue);
			}
			System.out.println("Degree-->" + key + "," + tsum);
		}
		// System.out.println(map);
	}

	static void degree_blocks(float deg, float mag, TreeMap<Integer, TreeMap<Integer, Integer>> map) {
		int block = (int) deg;
		int cur_mag_block = 0;
		int cur_deg_block = 0;

		if (360 >= block && block > 330) {
			cur_mag_block = magnitude_blocks(mag);
			cur_deg_block = 1;
		} else if (330 >= block && block > 300) {
			cur_mag_block = magnitude_blocks(mag);
			cur_deg_block = 2;
		} else if (300 >= block && block > 270) {
			cur_mag_block = magnitude_blocks(mag);
			cur_deg_block = 3;
		} else if (270 >= block && block > 240) {
			cur_mag_block = magnitude_blocks(mag);
			cur_deg_block = 4;
		} else if (240 >= block && block > 210) {
			cur_mag_block = magnitude_blocks(mag);
			cur_deg_block = 5;
		} else if (210 >= block && block > 180) {
			cur_mag_block = magnitude_blocks(mag);
			cur_deg_block = 6;
		} else if (180 >= block && block > 150) {
			cur_mag_block = magnitude_blocks(mag);
			cur_deg_block = 7;
		} else if (150 >= block && block > 120) {
			cur_mag_block = magnitude_blocks(mag);
			cur_deg_block = 8;
		} else if (120 >= block && block > 90) {
			cur_mag_block = magnitude_blocks(mag);
			cur_deg_block = 9;
		} else if (90 >= block && block > 60) {
			cur_mag_block = magnitude_blocks(mag);
			cur_deg_block = 10;
		} else if (60 >= block && block > 30) {
			cur_mag_block = magnitude_blocks(mag);
			cur_deg_block = 11;
		} else if (30 >= block && block >= 0) {
			cur_mag_block = magnitude_blocks(mag);
			cur_deg_block = 12;
		}
		TreeMap<Integer, Integer> internal_map = new TreeMap<>();
		int mag_count = 1;
		if (map.containsKey(cur_deg_block))
			internal_map = map.get(cur_deg_block);
		if (internal_map.containsKey(cur_mag_block))
			mag_count += internal_map.get(cur_mag_block);
		internal_map.put(cur_mag_block, mag_count);
		map.put(cur_deg_block, internal_map);
	}

	static int magnitude_blocks(float mag) {
		int block = (int) mag;

		if (46 >= block && block > 40) {
			return 1;
		} else if (40 >= block && block > 36) {
			return 2;
		} else if (36 >= block && block > 32) {
			return 3;
		} else if (32 >= block && block > 28) {
			return 4;
		} else if (28 >= block && block > 24) {
			return 5;
		} else if (24 >= block && block > 20) {
			return 6;
		} else if (20 >= block && block > 16) {
			return 7;
		} else if (16 >= block && block > 12) {
			return 8;
		} else if (12 >= block && block > 8) {
			return 9;
		} else if (8 >= block && block > 4) {
			return 10;
		} else if (4 >= block && block >= 0) {
			return 11;
		}
		return 0;
	}

}
