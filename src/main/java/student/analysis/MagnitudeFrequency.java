package student.analysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

public class MagnitudeFrequency extends Constants{

	public static void main(String[] args) throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub
		List<TreeMap<Integer,Integer>> list = new ArrayList<>();
		
		for (File file : Map_directory.listFiles()) {
			if (file.getName().startsWith("AD_Data")) {
				sequenceCalculator(file, list);
			}
		}
			for(Map.Entry<Integer, Integer> entry:list.get(0).entrySet()){
				System.out.println(entry.getKey()+","+entry.getValue());
			}
			System.out.println("-------------------------");
			for(Map.Entry<Integer, Integer> entry:list.get(1).entrySet()){
				System.out.println(entry.getKey()+","+entry.getValue());
			}
	}

	private static void sequenceCalculator(File file, List<TreeMap<Integer,Integer>> list)throws FileNotFoundException, IOException {
		TreeMap<Integer, Integer> inmap = new TreeMap<>();
		TreeMap<Integer, Integer> outmap = new TreeMap<>();
		Properties prop = new Properties();
		prop.load(new FileInputStream(file));
		for (String key : prop.stringPropertyNames()) {
			String value = prop.get(key).toString();
			String mag_deg[] = value.split(",");
			float deg = Float.valueOf(mag_deg[1]);
			float mag = Float.valueOf(mag_deg[0]);
			int bit = deg < 135 || deg > 300 ? 1 : 0;
			int count =1;
			if(bit ==1){
				if(outmap.containsKey((int)mag))
					count+= outmap.get((int)mag);
				outmap.put((int)mag, count);
			}else{
				if(inmap.containsKey((int)mag))
					count+= inmap.get((int)mag);
				inmap.put((int)mag, count);
			}
		}
		list.add(0,outmap);
		list.add(1,inmap);
	}

}
