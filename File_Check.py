from ghmm import *
sigma = IntegerRange(0, 32)

file = open("/home/chandra/Documents/Data/mapObjects/testing.seq",'r')
line=file.readline()

values = str(line).split(',')

del values[-1]

test_values = [int(x) for x in values if x.isdigit()]

test_seq = EmissionSequence(sigma, test_values)

file.close()    